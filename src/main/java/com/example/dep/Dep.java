package com.example.dep;

/**
 * Hello name!
 *
 */
public class Dep
{
    public static void main( String name )
    {
        System.out.println( "Hello " + name + "!" );
    }
}
