# FROM openjdk:11-jre-slim
# WORKDIR /app
# COPY . /app
# RUN mvn compile
# RUN mvn package artifactId='Dep.jar' com/example/dep/Dep.java
# CMD ["java", "com.example.dep.Dep"]

FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]

# FROM openjdk:8-jdk-alpine

# # Set the working directory within the container
# # WORKDIR /app

# # Copy the Maven project file and download dependencies
# # COPY pom.xml .


# # Copy the source code into the container
# # COPY src ./src
# ARG JAR_FILE=target/*.jar
# COPY ${JAR_FILE} app.jar
# # Build the application
# # RUN mvn package

# # Specify the command to run the Java application
# CMD ["java", "-jar", "target/Dep.jar"]